// Compile example: g++ implicit_method_query_6.cpp -o run

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <vector>
#include <cmath>

#define TIME 1
#define WIDTH 1

using namespace std;

typedef vector<double> vecD;

double DOUGLAS = 0;


double a_func(double pos, double dx) {
    return 1.0;
}

double initialize_func(vector<vecD> &U_tme, int N, double dx) {
    for (int i = 0; i < N; ++i) {
        U_tme[i][0] = 1; // initial value for x

        if (i == 0 || i == N - 1) {
            U_tme[i][1] = U_tme[i][0];
        } else {
            U_tme[i][1] = 0;
        }
    }
}

void solve_system(vector<vecD> &U_tme, int N, double r, double dx) {
    vecD result(N, 0), b_fact(N, 0), d_fact(N, 0);
    
    // calculate right side
    for (int i = 1; i < N - 1; ++i) {
        double pos_1 = ((double)i - 1.0) / 2.0;
        double pos_2 = ((double)i + 1.0) / 2.0;

        result[i] += (r / 2.0 + DOUGLAS / 2.0) * a_func(pos_1, dx) * U_tme[i - 1][0];
        result[i] += (1.0 - (r / 2.0 + DOUGLAS / 2.0) * (a_func(pos_1, dx) + a_func(pos_2, dx))) * U_tme[i][0];
        result[i] += (r / 2.0 + DOUGLAS / 2.0) * a_func(pos_2, dx) * U_tme[i + 1][0];

        if (i == 1) {
            result[i] += (r / 2.0 - DOUGLAS / 2.0) * a_func(pos_1, dx) * U_tme[0][1];
        } else if(i == N - 2) {
            result[i] += (r / 2.0 - DOUGLAS / 2.0) * a_func(pos_2, dx) * U_tme[N - 1][1];
        }
    }
    
    // solve system
    b_fact[1] = 1.0 + (r / 2.0 - DOUGLAS / 2.0) * (a_func(0, dx) + a_func(1, dx));
    d_fact[1] = result[1];
    
    for (int i = 2; i < N - 1; ++i) {
        double pos_1 = ((double)i - 1.0) / 2.0;
        double pos_2 = ((double)i + 1.0) / 2.0;
        
        double m_i = ((r / 2.0 - DOUGLAS / 2.0) * a_func(pos_1, dx)) / b_fact[i - 1];

        b_fact[i] = 1.0 + (r / 2.0 - DOUGLAS / 2.0) * (a_func(pos_1, dx) + a_func(pos_2, dx));
        b_fact[i] += m_i * (-r / 2.0 + DOUGLAS / 2.0) * a_func(pos_2, dx);

        d_fact[i] = result[i] + m_i * d_fact[i - 1];
    }

    for (int i = N - 2; i >= 1; --i) {
        double pos_2 = ((double)i + 1.0) / 2.0;
        
        if (i == N - 2) {
            U_tme[i][1] = d_fact[i] / b_fact[i];
        } else {
            U_tme[i][1] = (d_fact[i] + (r / 2.0 - DOUGLAS / 2.0) * a_func(pos_2, dx) * U_tme[i + 1][1]) / b_fact[i];
        }
    }

    U_tme[0][1] =  U_tme[1][1] / (1.0 + dx); //  boundary value for t
    U_tme[N - 1][1] = U_tme[N - 2][1] / (1.0 + dx); //  boundary value for t

    for (int i = 0; i < N; ++i) {
        U_tme[i][0] = U_tme[i][1];
        
        if (i == 0 || i == N - 1) {
            U_tme[i][1] = U_tme[i][0];
        } else {
            U_tme[i][1] = 0;
        }
    }
}


int main() {
    int choice = 0;

    do {
        printf("CRANK-NICOLSON(1) / DOUGLAS(2): ");
        scanf("%d", &choice);

        if (choice != 1 && choice != 2) {
            printf("Please enter '1' or '2'\n");
        }
    } while (choice != 1 && choice != 2);
    
    if (choice == 2) { // DOUGLAS method is selected
        DOUGLAS = 1.0 / 6.0;
    }

    double r = 0.2, dx = 0.1;
    double dt = r * dx * dx;
    const int N = (int)(WIDTH / dx + 1);
    vector<vecD> U_tme(N, vecD(2, 0)); 
    
    initialize_func(U_tme, N, dx);
    
    cout.precision(5);
    cout << "            ";

    for (int i = 0; i < N; ++i) {
        cout << left << setw(10) << fixed << i * dx << " "; 
    }

    cout << endl << "         ";
    
    for (int i = 0; i < N - 1; ++i) {
        cout << "------------"; 
    }
    
    cout << endl;

    for (int t = 1; (double)t * dt <= TIME + 10e-9; ++t) {
        solve_system(U_tme, N, r, dx);
        cout << fixed << t * dt << "  |  ";
        
        for (int i = 0; i < N; ++i) {
            cout << left << setw(10) << fixed << U_tme[i][0] << " "; 
        }

        cout << endl;
    }

    return 0;
}

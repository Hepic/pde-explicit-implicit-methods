// Compile example: g++ explicit_method.cpp -o run

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <vector>
#include <cmath>

#define TIME 0.02
#define WIDTH 1

using namespace std;

typedef vector<double> vecD;


double a_func(double pos, double dx) {
    return 1.0;
}

double initialize_func(vector<vecD> &U_tme, int N, double dx) {
    for (int i = 0; i < N; ++i) {
        U_tme[i][0] = 0.5; // initial value for x

        if (i == 0 || i == N - 1) {
            U_tme[i][1] = 0; // boundary value for t
        } else {
            U_tme[i][1] = 0;
        }
    }
}

void matrix_mult(vector<vecD> &U_tme, int N, double r, double dx) {
    for (int i = 1; i < N - 1; ++i) {
        double pos_1 = ((double)i - 1.0) / 2.0;
        double pos_2 = ((double)i + 1.0) / 2.0;

        U_tme[i][1] += r * a_func(pos_1, dx) * U_tme[i - 1][0];
        U_tme[i][1] += (1.0 - r * (a_func(pos_1, dx) + a_func(pos_2, dx))) * U_tme[i][0];
        U_tme[i][1] += r * a_func(pos_2, dx) * U_tme[i + 1][0];
    }

    for (int i = 0; i < N; ++i) {
        U_tme[i][0] = U_tme[i][1];
        
        if (i == 0 || i == N - 1) {
            U_tme[i][1] = 0; // boundary value for t
        } else {
            U_tme[i][1] = 0;
        }
    }
}


int main() {
    double r = 0.1, dx = 0.1;
    double dt = r * dx * dx;
    const int N = (int)(WIDTH / dx + 1);
    vector<vecD> U_tme(N, vecD(2, 0)); 
    
    initialize_func(U_tme, N, dx);
    
    cout.precision(5);
    cout << "            ";

    for (int i = 0; i < N; ++i) {
        cout << left << setw(10) << fixed << i * dx << " "; 
    }

    cout << endl << "         ";
    
    for (int i = 0; i < N - 1; ++i) {
        cout << "------------"; 
    }
    
    cout << endl;

    for (int t = 1; (double)t * dt <= TIME + 10e-9; ++t) {
        matrix_mult(U_tme, N, r, dx); // find next time line    
        cout << fixed << t * dt << "  |  ";

        for (int i = 0; i < N; ++i) {
            cout << left << setw(10) << fixed << U_tme[i][0] << " "; 
        }

        cout << endl;
    }

    return 0;
}
